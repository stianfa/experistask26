﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task24.Models
{
    public class SupervisorGroup
    {
        public static List<Supervisor> CurrentSupervisors { get; } = new List<Supervisor>();

        public static void AddSupervisor(Supervisor newSupervisor)
        {
            CurrentSupervisors.Add(newSupervisor);
        }
    }
}
