﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Task24.Models
{
    public class Supervisor
    {

        // Would not ask for this if we had a db-model, but doing it now just to have another field
        [Required(ErrorMessage = "Please enter a number")]
        [RegularExpression("^\\d+$", ErrorMessage = "Please enter a valid whole number")]
        public int? Id { get; set; } = 404;

        [Required(ErrorMessage = "Please enter a name")]
        public string Name { get; set; } = "Name not supplied";



        public static List<Supervisor> GetSampleSupervisorData()
        {
            Supervisor sup1 = new Supervisor { Id = 1, Name = "Bob" };
            Supervisor sup2 = new Supervisor { Id = 2, Name = "Pete" };
            Supervisor sup3 = new Supervisor { Id = 3, Name = "sob" };
            Supervisor sup4 = new Supervisor { Name = "Slob" };
            Supervisor sup5 = new Supervisor { Id = 2};
            Supervisor sup6 = new Supervisor();
            Supervisor sup7 = null;

            return new List<Supervisor> { sup1, sup2, sup3, sup4, sup5, sup6, sup7};
        }
    }
}
