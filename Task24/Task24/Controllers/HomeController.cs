﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task24.Models;

namespace Task24.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index() => View("AllSupervisors", Supervisor.GetSampleSupervisorData());


        public IActionResult SupervisorsWithS() => View(Supervisor.GetSampleSupervisorData().Where(x => x?.Name[0] == 's' || x?.Name[0] == 'S'));
    }
}
